#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self): # Case where the integers are the same
        s = "7 7\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 7)
        self.assertEqual(j, 7)

    def test_read_3(self): # Case where the integers are not from least to greatest
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    def test_read_4(self): # Added Test Case
        s = "9 13\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9)
        self.assertEqual(j, 13)
    def test_read_5(self): # Case where the integers are large
        s = "800 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 800)
        self.assertEqual(j, 10000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self): # Case where the integers are the same
        v = collatz_eval(7, 7)
        self.assertEqual(v, 17)

    def test_eval_6(self): # Case where the integers are not from least to greatest
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
    
    def test_eval_7(self): # Added Test Case
        v = collatz_eval(9, 13)
        self.assertEqual(v, 20)

    def test_eval_8(self): # Case where the integers are large
        v = collatz_eval(800, 10000)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self): # Case where the integers are the same
        w = StringIO()
        collatz_print(w, 7, 7, 17)
        self.assertEqual(w.getvalue(), "7 7 17\n")
    
    def test_print_3(self): # Case where the integers are not from least to greatest
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    def test_print_4(self): # Added Test Case
        w = StringIO()
        collatz_print(w, 9, 13, 20)
        self.assertEqual(w.getvalue(), "9 13 20\n")
    
    def test_print_5(self): # Case where the integers are large
        w = StringIO()
        collatz_print(w, 800, 10000, 262)
        self.assertEqual(w.getvalue(), "800 10000 262\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self): # Case where the reader has only 1 set of integers
        r = StringIO("9 13\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
                w.getvalue(), "9 13 20\n")

    def test_solve_3(self): # Case where we add all the previous test cases (including corner cases)
        r = StringIO("7 7\n10 1\n9 13\n800 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
                w.getvalue(), "7 7 17\n10 1 20\n9 13 20\n800 10000 262\n")
    
    def test_solve_4(self): # Same as test_solve_3 except we change the order
        r = StringIO("800 10000\n9 13\n10 1\n7 7\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
                w.getvalue(), "800 10000 262\n9 13 20\n10 1 20\n7 7 17\n")

    # ------------
    # cycle_length
    # ------------
    
    def test_cycle_length_1(self): # Testing the case where n = 1, without memo
        c = cycle_length(1)
        self.assertEqual(c, 1)

    def test_cycle_length_2(self): # Testing the case where n = 1, with memo
        c = cycle_length(1, {1:1})
        self.assertEqual(c, 1)

    def test_cycle_length_3(self): # Testing a value n != 1, without memo
        c = cycle_length(7)
        self.assertEqual(c, 17)

    def test_cycle_length_4(self): # Testing a value n != 1, where n in memo
        c = cycle_length(7, {7:17})
        self.assertEqual(c, 17)

    def test_cycle_length_5(self): # Testing another value n != 1, without memo
        c = cycle_length(10)
        self.assertEqual(c, 7)

    def test_cycle_length_6(self): # Testing a value n != 1, where an intermediate value is in memo.
        c = cycle_length(10, {2:2})
        self.assertEqual(c, 7)

    def test_cycle_length_7(self): # Testing n = 2
        c = cycle_length(2)
        self.assertEqual(c, 2)


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
